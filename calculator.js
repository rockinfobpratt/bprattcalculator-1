//TODO add keyboard entry, make the screen reset if a new sum is being added.

//declare variables
var firstNum = [];
var secondNum = [];
var operator = "";
var mathArray = [];


//data integrity check, this will be used for keyboard entry
/**
 *
 * @param num
 * @returns {boolean}
 */
function isint(num) {
    return typeof num === "number";
}


//math functions
/**
 *
 * @param numOne
 * @param numTwo
 * @returns {number}
 */
function add(numOne, numTwo) {
    if (isint(numOne) && isint(numTwo)) {
        return numOne + numTwo;
    }
    else {
        return 0;
    }
}
/**
 *
 * @param numOne
 * @param numTwo
 * @returns {number}
 */
function multiply(numOne, numTwo) {
    if (isint(numOne) && isint(numTwo)) {
        return numOne * numTwo;
    }
    else {
        return 0;
    }
}

/**
 *
 * @param numOne
 * @param numTwo
 * @returns {number}
 */
function divide(numOne, numTwo) {
    if (isint(numOne) && isint(numTwo)) {
        return numOne / numTwo;
    }
    else {
        return 0;
    }
}
/**
 *
 * @param numOne
 * @param numTwo
 * @returns {number}
 */
function subtract(numOne, numTwo) {
    if (isint(numOne) && isint(numTwo)) {
        return numOne - numTwo;
    }
    else {
        return 0;
    }
}

//add event listeners to all the numbers
$(".calculatorButtons").on("click", function (evt) {
    if (operator === "") {
        firstNum.push(evt.target.id);
    } else {
        secondNum.push(evt.target.id);
    }
    document.getElementById("answerDiv").value += evt.target.id;
});

//add event listener to the clear function
$(".clear").on("click", function () {
    document.getElementById("answerDiv").value = "";
    clear();
});

//add event listeners to all the operands

$(".operand").on("click", function (evt) {
    var value = evt.target.id;
    var answer;

    if (value === "equals") {
        mathArray.push(firstNum, operator, secondNum);
        //get numbers as opposed to arrays
        var first = arrayToInt(firstNum);
        var second = arrayToInt(secondNum);

        answer = doMath(first, operator, second);

        //clear the array
        clear();
        //print to screen
        printToDisplay(answer);
    } else {

        //if this is a multipart sum then make the new result of firstnum the answer and clear the operator and seocond array
        if (secondNum.length > 0) {
            var first = arrayToInt(firstNum);
            var second = arrayToInt(secondNum);
            firstNum = [];
            firstNum.push(doMath(first, operator, second));
            operator ="";
            secondNum = [];
            printToDisplay(firstNum[0]);
        } else {
            operator = value;
            printToDisplay("");
        }
        operator = value;
        printToDisplay("");
    }
});

//clear function

function clear() {
    firstNum = [];
    secondNum = [];
    mathArray = [];
    operator = "";
}

//this part does the maths
/**
 *
 * @param first
 * @param second
 * @returns {number}
 */
function doMath(first, operator, second) {
    var answer;
    switch (operator) {
        case "multiply":
            answer = multiply(first, second);
            break;
        case "subtract":
            answer = subtract(first, second);
            break;
        case "add":
            answer = add(first, second);
            break;
        case "divide":
            answer = divide(first, second);
            break;
    }
    return answer;
}

//convert the number array to a number for instance "1","2" to 12
/**
 *
 * @param numArray
 * @returns {Number}
 */
function arrayToInt(numArray) {
    var number = "";
    numArray.forEach(function (num) {
        number += num;
    });

    return parseInt(number);
}

//print the answer to the console
function printToDisplay(answer) {
    document.getElementById("answerDiv").value = answer;
}


//Warm up exercises below
/**
 *
 *  @param numArray
 *  @returns {number}
 **/
function my_max(numArray) {
    var highest = 0;
    numArray.forEach(function (num) {
        if (num > highest) {
            highest = num;
        }
    });
    return highest;
}
/**
 *
 * @param text
 * @returns {number}
 */
function vowel_count(text) {
    var vowels = ["A", "E", "I", "O", "U", "Y"];
    var vowelCount = 0;
    for (var i = 0; i < text.length; i++) {
        if (jQuery.inArray(text.charAt(i).toUpperCase(), vowels) !== -1) {
            vowelCount++;
        }
    }
    return vowelCount;
}
/**
 *
 * @param reversable
 * @returns {string}
 */
function reverseString(reversable) {
    var result = "";
    for (var i = reversable.length - 1; i > -1; i--) {
        result += reversable.charAt(i);
    }
    return result;
}

//print the warm up results to console

//return highest number in an array
console.log(my_max([1, 2, 32, 4]));
//return the amount of vowels in a string
console.log(vowel_count("SEVEN"));
//return a reversed version of the string
console.log(reverseString("this is a string"));
