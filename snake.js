//TODO write an eat function, write a function that deposits food on a random location
//todo sort out growth
//TODO do up a restart ui

var INCREASE_SPEED_AFTER_MS = 3000;

var Direction = {
    NORTH: 0,
    EAST: 1,
    SOUTH: 2,
    WEST: 3
};
var gameLoopTimer;
var snake = {
    x: 20,
    y: 20,
    dir: Direction.EAST,
    speed: 5,
    tail: []
};
var timePlayedMs = 0;

$(document).ready(function () {
    $("#board").focus();

    render();
    gameLoopTimer = window.setInterval(gameLoop, 1000 / snake.speed);
});

function render() {
    console.log(snake);

    $("#board").empty();
    for (var x = 0; x < 40; x++) {
        var row = $('<div class="row">');
        for (var y = 0; y < 40; y++) {
            var content = "";
            if (snake.x == x && snake.y == y) {
                content = "O";

            } else {
                for (var tailKey in snake.tail) {
                    if (snake.tail[tailKey].x == x && snake.tail[tailKey].y == y) {
                        content = "x";

                        break;
                    }
                }
            }
            row.append("<div class='bodycell'> " + content + "</div>");
        }
        $("#board").append(row);
    }
}

function gameLoop() {
    move();
    render();
    recordTime();
    dieOrLive();
}

function dieOrLive() {
    if (snake.x > 39 || snake.x < 0 || snake.y > 39 || snake.y < 0) {
        clearInterval(gameLoopTimer);
        alert("you suck");
    }
}

function recordTime() {
    timePlayedMs += 1000 / snake.speed;
    if (timePlayedMs > INCREASE_SPEED_AFTER_MS) {
        clearInterval(gameLoopTimer);
        snake.speed++;

        gameLoopTimer = window.setInterval(gameLoop, 1000 / snake.speed);
    }
}


function move() {

    switch (snake.dir) {
        case Direction.NORTH:
            snake.x--;
            break;
        case Direction.EAST:
            snake.y++;
            break;
        case Direction.SOUTH:
            snake.x++;
            break;
        case Direction.WEST:
            snake.y--;
            break;
    }


    snake.tail.shift({
        x: snake.x,
        y: snake.y
    });
    if (snake.tail.length > snake.speed){
        snake.tail.pop();
    }

}





$(document).keydown(function () {

    // left, up, right, down
    if (event.which == 37 || event.which == 38 || event.which == 39 || event.which == 40) {
        event.preventDefault();
    }

    // left
    if (event.which == 37) {
        snake.dir--;
    }

    // right
    else if (event.which == 39) {
        snake.dir++;
    }

    if (snake.dir < 0) {
        snake.dir = 3;
    }

    if (snake.dir > 3) {
        snake.dir = 0;
    }
});









